package com.company.experience;

import java.util.Date;

public class FujitsuDeveloper extends WorkExperience {
    public FujitsuDeveloper(Date startDate, Date endDate) {
        super(startDate, endDate);
    }

    @Override
    public String workResponsibilities() {
        StringBuilder stringBuilder = new StringBuilder();
        return stringBuilder
                .append("Active development for Fujitsu`s solutions. \n" +
                        "That includes providing new features, bugfixing, application scalability.\n" +
                        "First integrates Jira and inner ticket system (springboot).\n" +
                        "Second is a Vmware plugin for Host health Monitoring.\n")
                .append("* Developing springboot and springframework application.\n")
                .append("* Providing support for less experienced team members.\n")
                .append("* Creating developers environments.\n")
                .append("* Lead for creating basic daily tests environment and test.\n")
                .append("* Acceptance critera creation.\n")
                .toString();
    }
}
