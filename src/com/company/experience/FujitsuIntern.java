package com.company.experience;

import java.util.Date;

public class FujitsuIntern extends WorkExperience {
    public FujitsuIntern(Date startDate, Date endDate) {
        super(startDate, endDate);
    }

    @Override
    public String workResponsibilities() {
        StringBuilder stringBuilder = new StringBuilder();
        return stringBuilder
                .append("* Running manual test cases for Java desktop app.\n")
                .append("* Active communication with developers team for quality improvement.\n")
                .append("* Creating automation environment, which included jenkins, test cases, Ranorex (QA tool) configuration.\n")
                .toString();
    }
}
