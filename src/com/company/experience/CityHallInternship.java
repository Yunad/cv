package com.company.experience;

import java.util.Date;

public class CityHallInternship extends WorkExperience {
    public CityHallInternship(Date startDate, Date endDate) {
        super(startDate, endDate);
    }

    @Override
    public String workResponsibilities() {
        StringBuilder stringBuilder = new StringBuilder();
        return stringBuilder
                .append("* Office rooms network configuration \n")
                .append("* RJ-45 cable creation with high latency")
                .append("* Providing support for less experienced team members.\n")
                .append("* Office rooms support, which included:\n" +
                        " printer configuration,\n" +
                        "overall business application support.\n")
                .toString();
    }
}
