package com.company;

import com.company.experience.WorkExperience;

import java.util.ArrayList;
import java.util.List;

public class Person {
    private String name;
    private String lastName;
    private String phoneNumber;
    private String title;
    private String profile;
    private List<String> softSkills = new ArrayList<>();
    private List<String> hardSkills = new ArrayList<>();
    private List<String> certificates = new ArrayList<>();
    private List<String> hobbies = new ArrayList<>();
    private List<WorkExperience> experiences = new ArrayList<>();

    public Person(String name, String lastName, String phoneNumber, String title) {
        this.name = name;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<String> getSoftSkills() {
        return softSkills;
    }

    public void setSoftSkills(List<String> softSkills) {
        this.softSkills = softSkills;
    }

    public List<String> getHardSkills() {
        return hardSkills;
    }

    public void setHardSkills(List<String> hardSkills) {
        this.hardSkills = hardSkills;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public List<String> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<String> certificates) {
        this.certificates = certificates;
    }

    public List<String> getHobbies() {
        return hobbies;
    }

    public void setHobbies(List<String> hobbies) {
        this.hobbies = hobbies;
    }

    public List<WorkExperience> getExperiences() {
        return experiences;
    }

    public void setExperiences(List<WorkExperience> experiences) {
        this.experiences = experiences;
    }

    public String description() {
        StringBuilder stringBuilder = new StringBuilder();
        return stringBuilder
                .append(getName() + "\n")
                .append(getLastName() + "\n")
                .append(getPhoneNumber() + "\n")
                .append(getExperiences().toString() + "\n")
                .append(getSoftSkills() + "\n")
                .append(getHardSkills() + "\n")
                .append(getCertificates() + "\n")
                .append(getHobbies() + "\n")
                .toString();
    }
}
