package com.company;

import com.company.experience.CityHallInternship;
import com.company.experience.FujitsuDeveloper;
import com.company.experience.FujitsuIntern;
import com.company.experience.WorkExperience;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        WorkExperience fujitsuIntern = new FujitsuIntern(new Date(2017, 2, 10), new Date(2018, 2, 10));
        WorkExperience fujitsuDeveloper = new FujitsuDeveloper(new Date(2018, 2, 11), new Date(2019, 9, 6));
        WorkExperience cityHallInternship = new CityHallInternship(new Date(2016, 6, 1), new Date(2016, 7, 1));
        List<WorkExperience> workExperiences = Arrays.asList(cityHallInternship, fujitsuIntern, fujitsuDeveloper);
        Person adam = createAdam(workExperiences);
        System.out.println(adam.description());
    }

    private static Person createAdam(List<WorkExperience> experiences) {
        Person adam = new Person("Adam", "Kaczmarek", "791-491-542", "Software developer");
        adam.setCertificates(Arrays.asList("TOEIC", "web application developer", "IT technician", "Scrum Master"));
        adam.setHardSkills(Arrays.asList("IDEA Ultimate usage",
                "Basics of GIT/Jenkins/Linux/Networks/SpringBoot/Springframework/TestNG/Maven/SOA/Jira",
                "Basic usage of VMware", "Scrum/Kanban", "Very good English"));
        adam.setSoftSkills(Arrays.asList("Self awarness", "High communication skills", "Precision", "Fast learner",
                "Team work player", "Analytical thinking", "Time management skills", "Problem solver"));
        adam.setHobbies(Arrays.asList("PS4 games", "music", "fitness/gym", "self development", "IT", "philosophy/psychology"));
        adam.setProfile("I am dedicated and determined developer. As one of the my goes is to become  become a professional in development. When I am working with code, I have in mind standards, functionality, testability.\n" +
                "\tPractics like CI/CD are not unknown topics for me. I value communiation, flexibility, transparency . Therefore Scrum Team s are teams which i prefer. I am not afraif of challenging status Quo.");
        adam.setExperiences(experiences);
        return adam;
    }
}
