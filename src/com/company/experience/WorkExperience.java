package com.company.experience;

import java.util.Date;

public abstract class WorkExperience {
    private Date startDate;
    private Date endDate;

    protected WorkExperience(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String workDescription() {
        StringBuilder stringBuilder = new StringBuilder();
        return stringBuilder
                .append("Started: " + startDate + "\n")
                .append("ended: " + endDate + "\n")
                .append("work description: \n" + workResponsibilities())
                .toString();
    }

    public abstract String workResponsibilities();

    @Override
    public String toString() {
        return "work: " + this.getClass().getSimpleName() + " \n" + workDescription();
    }
}
